import React from 'react'
import './index.css';
import ReactDOM from 'react-dom';
import logger from 'redux-logger';
import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga'
import Root from './root'
import mySaga from './sagas/index';
import {tweetsReducer, sidebarReducer} from './reducers';

// Build the middleware for intercepting and dispatching navigation actions
const router = routerMiddleware(history)
const sagas = createSagaMiddleware()
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  combineReducers({
    sidebar: sidebarReducer,
    tweets: tweetsReducer,
    router: routerReducer,
  }),
  composeEnhancers(applyMiddleware(sagas, router, logger))
)

sagas.run(mySaga)

// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

ReactDOM.render(
  <Root store={store} history={history} />,
  document.getElementById('root')
)
