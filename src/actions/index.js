import { TWEET_FETCH_REQUEST,
   SHOW_TWEET_INPUT_ADD,
   ADD_TWEET_FEED,
   INPUT_CHANGE,
   TWEET_FETCH_SUCCESS,
   TWEET_FETCH_FAILURE
} from '../constants';

export const requestTweets = username => ({ type: TWEET_FETCH_REQUEST, payload: username })

export const showTweetFeedInput = () => ({ type: SHOW_TWEET_INPUT_ADD })

export const addTweetFeed = usernameValue => ({ type: ADD_TWEET_FEED, payload: usernameValue })

export const inputChange = value => ({ type: INPUT_CHANGE, payload: value })

export const tweetFetchSuccess = (tweets) => ({ type: TWEET_FETCH_SUCCESS, tweets })

export const tweetFetchFailure = (message) => ({ type: TWEET_FETCH_FAILURE, message: message })

