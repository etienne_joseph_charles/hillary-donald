export const isSuccessfulResponse = (resp) => resp.status >= 200 && resp.status < 300

export const getJsonResponse = (resp) => resp.json()

export const checkResponseStatusIsOk = (resp) => {
  return (isSuccessfulResponse(resp))
    ? Promise.resolve(resp)
    : getJsonResponse(resp).then(err => Promise.reject(new Error(err.message)))
}
