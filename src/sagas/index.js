import { call, put, takeEvery } from 'redux-saga/effects'
import { TWEET_FETCH_REQUEST } from '../constants/index';
import { getJsonResponse, checkResponseStatusIsOk } from './utils'
import SERVER_LOCATION from './constants'
import { tweetFetchSuccess, tweetFetchFailure } from '../actions';

function* mySaga() {
  yield takeEvery(TWEET_FETCH_REQUEST, fetchTweets);
}

function* fetchTweets(action) {
  const { tweets, error } = yield call(getTweetsForUser, action.payload)
  yield handleTweets(tweets, error)
}

export const getTweetsForUser = (name) => {
  const baseUrl = SERVER_LOCATION + name;
  return fetch(baseUrl)
    .then(checkResponseStatusIsOk)
    .then(getJsonResponse)
    .then(tweets => ({ tweets }))
    .catch(error => ({ error }))
}

function* handleTweets(tweets, error) {
  (tweets) ?
    yield put(tweetFetchSuccess(tweets)) :
    yield put(tweetFetchFailure(error.message))
}

export default mySaga;
