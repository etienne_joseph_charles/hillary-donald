import {TWEET_FETCH_REQUEST, TWEET_FETCH_SUCCESS, TWEET_FETCH_FAILURE} from '../constants/index';

const initialState = {
  isFetching: false,
  items: []
}
const tweetsReducer = (state = initialState, action) => {
  switch (action.type) {
    case TWEET_FETCH_REQUEST:
      return {
        ...state,
        isFetching: true
      }
    case TWEET_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.tweets,
        errorMessage: ''
      }
    case TWEET_FETCH_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.message,
        items:[]
      }

    default:
      return state;
  }
};
export default tweetsReducer;
