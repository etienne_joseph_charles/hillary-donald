import { SHOW_TWEET_INPUT_ADD, ADD_TWEET_FEED, INPUT_CHANGE } from '../constants';

const initialState = {
  currentTweetFeedInput: '',
  inputIsVisible: false,
  feeds: [ {
    id: 1,
    screenName: 'realDonaldTrump',
    displayName: 'Donald Trump'
  }, {
    id: 2,
    screenName: 'HillaryClinton',
    displayName: 'Hillary Clinton'
  }]
}

const sidebarReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_TWEET_INPUT_ADD:
      return {
        ...state,
        inputIsVisible: true
      }
    case ADD_TWEET_FEED:
        let newId = state.feeds.length;
      const feeds = [ ...state.feeds, {
        id: newId += 1,
        screenName: action.payload
      }]
      return {
        ...state,
        feeds
      }
    case INPUT_CHANGE:
      return {
        ...state,
        currentTweetFeedInput: action.payload
      }
    default:
      return state;
  }
};
export default sidebarReducer;
