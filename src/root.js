import React, { PropTypes } from 'react'
import App from './containers/App';
import createHistory from 'history/createBrowserHistory';
import { Provider } from 'react-redux';
import { Route, Redirect, Switch} from 'react-router';
import { ConnectedRouter } from 'react-router-redux';

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory()

const Root = ({ store }) => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Switch>
          <Route path="/:username" component={App}/>
          <Redirect from="/" to="realDonaldTrump" />
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
};

export default Root;