import React, { Component, PropTypes } from 'react';
import './App.css';
import 'normalize-css';
import { connect } from 'react-redux';
import { requestTweets } from '../actions/index';
import Header from '../components/Header';
import Tweetlist from '../components/Tweetlist';
import Sidebar from '../components/Sidebar'
import Loader from '../components/Loader';

class App extends Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.username !== this.props.match.params.username) {
      const { dispatch } = nextProps
      dispatch(requestTweets(nextProps.match.params.username))
    }
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(requestTweets(this.props.match.params.username))
  }

  render() {
    const { tweets, message, isFetching, location, feeds } = this.props
    return (
      <div className="App">
        <Loader isFetching={isFetching} />
        <Header />
        <div className="Container">
          <Sidebar location={location} feeds={feeds}/>
          <Tweetlist tweets={tweets} errorMessage={message} />
        </div>
      </div>
    );
  }
  static propTypes = {
    tweets: PropTypes.array.isRequired
  }
}
const mapStateToProps = state => {
  return {
    tweets: state.tweets.items,
    feeds: state.sidebar.feeds,
    message: state.tweets.errorMessage,
    isFetching: state.tweets.isFetching
  }
}

export default connect(mapStateToProps)(App);
