import React from 'react';
import '../../../node_modules/spinkit/css/spinkit.css';
import './Loader.css';

const Loader = (props) => {
    const {isFetching} = props;
    return (
        (isFetching)
        ? <div className="Loader">
            <div className="sk-wandering-cubes">
                <div className="sk-cube sk-cube1"></div>
                <div className="sk-cube sk-cube2"></div>
            </div>
        </div>
        : null
    );
};

export default Loader;