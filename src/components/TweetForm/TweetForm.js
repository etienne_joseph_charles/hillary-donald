import React from 'react';
import './TweetForm.css';

const TweetForm = (props) => {
  let input;
  return (
    <div className="TweetForm">
      <form onSubmit={
        (event) => {
          event.preventDefault();
          if (input.value) {
            props.addTweetFeed(input.value)
          }
        }}>
        <input ref={node => input = node} type="text" className={props.isVisible ? '' : 'hidden'} onChange={(event) => props.onInputChange(event.target.value)} />
      </form>
      <button onClick={props.showTweetFeedInput}>+</button>
    </div>
  );
};

export default TweetForm;
