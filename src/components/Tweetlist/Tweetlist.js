import React from 'react';
import TweetItem from './TweetItem';
import './Tweetlist.css'

const Tweetlist = (props) => {
  const hasError = props.errorMessage ? 'Error': '';
  return (
    <div className="Tweetlist">
      <p className={hasError}>{props.errorMessage}</p>
      <ul>
        {props.tweets.map(tweet => <TweetItem key={tweet.id} {...tweet} />)}
      </ul>
    </div>
  );
};

export default Tweetlist;
