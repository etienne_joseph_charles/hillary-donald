import moment from 'moment';

export const displayTimeFromNow = (date) => moment(date).fromNow();
