import React from 'react';
import {displayTimeFromNow} from './utils'
import './TweetItem.css'

const TweetItem = (props) => {
    return (
        <li className="TweetItem">
            <div className="description">{props.description}</div>
            <span className="date">{displayTimeFromNow(props.date)}</span>
        </li>
    );
};

export default TweetItem;