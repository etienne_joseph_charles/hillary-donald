import React from 'react';
import { NavLink } from 'react-router-dom';

const Feed = (feed) => {
  return (
    <NavLink to={{pathname: `/${feed.screenName}`}} activeClassName="selected">{feed.displayName || feed.screenName}</NavLink>
  );
};

export default Feed;
