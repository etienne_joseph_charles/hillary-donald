import React from 'react';
import Feed from './Feed';

const FeedList = (props) => {
  return (
    <div className="FeedList">
        {props.feeds.map(feed => <Feed key={feed.id} {...feed} /> )}
      </div>
  );
};

export default FeedList;
