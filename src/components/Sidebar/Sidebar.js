import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showTweetFeedInput, addTweetFeed, inputChange } from '../../actions';
import TweetForm from '../TweetForm/TweetForm';
import FeedList from './FeedList/FeedList';
import './Sidebar.css'

class Sidebar extends Component {
  render() {
    const { inputIsVisible, inputChange, addTweetFeed, showTweetFeedInput } = this.props;
    return (
      <div className="Sidebar">
        <FeedList feeds={this.props.feeds} />
          <TweetForm
            isVisible={inputIsVisible}
            addTweetFeed={addTweetFeed}
            showTweetFeedInput={showTweetFeedInput}
            onInputChange={inputChange}
          />
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { inputIsVisible: state.sidebar.inputIsVisible }
}

const mapDispatchToProps = (dispatch) => {
  return {
    inputChange: (value) => dispatch(inputChange(value)),
    showTweetFeedInput: () => dispatch(showTweetFeedInput()),
    addTweetFeed: (value) => dispatch(addTweetFeed(value))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
