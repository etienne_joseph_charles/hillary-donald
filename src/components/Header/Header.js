import './Header.css'
import React from 'react';

const Header = () => {
  return (
    <nav className="Header">
        Tweets App
    </nav>
  );
}
export default Header