import express from "express";
const cors = require('cors')
import { getTweets } from './TweetsService'
const app = express()
app.use(cors())

app.all('/', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
})

app.get('/', (req, res) => {
  console.log(req.query.users);
})

app.get('/:username', (req, res) => {
  let username = req.params.username
  res.setHeader('Content-Type', 'application/json');
  getTweets(username)
    .fork(
      error => res.status(404).send(JSON.stringify(error)),
      tweets => res.send(JSON.stringify(tweets))
    )
});
app.listen(4000)


